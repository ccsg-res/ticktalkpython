# Copyright 2022 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import argparse

from ticktalkpython import DebugLogger
from ticktalkpython import Ensemble
from ticktalkpython.IPC import *


def run_application_client(name, self_ip, self_port, rtm_ip, rtm_port, timeout,
                           log_file_name):

    rtm_address = rtm_ip + ":" + str(rtm_port)

    # TODO: how should an ensemble encode an output function?
    # route to a runtime manager?
    ens1 = Ensemble.TTEnsemble(log_file_name, name, None)
    ens1.setup_queues(is_sim=False)
    ens1.setup_physical_processes(network_ip=self_ip,
                                  rx_network_port=self_port,
                                  tx_network_port=self_port + 1)
    ens1.connect_to_TickTalk_network(rtm_address)

    if timeout <= 0:
        ens1.enter_steady_state()
    else:
        ens1.enter_steady_state(timeout=timeout)


def main():
    parser = argparse.ArgumentParser(
        description='instantiate an ensemble for a TTPython program')

    parser.add_argument('name', help="the name of the ensemble")
    parser.add_argument(
        '--ip',
        default='127.0.0.1',
        help='the ip of the ensemble (default: localhost:127.0.0.1)')
    parser.add_argument('--rtm_ip',
                        default='127.0.0.1',
                        help=("the runtime manager's ip to connect to"
                              " (default: localhost:127.0.0.1)"))
    parser.add_argument(
        'port',
        type=int,
        help='the port of the ensemble. reserves both the port number p and p+1'
    )
    parser.add_argument('--rtm_port',
                        type=int,
                        required=True,
                        help='the port of the runtime manager')
    parser.add_argument(
        '--timeout',
        default=60,
        type=int,
        help='ensemble timeout (default: 60 (sec), 0 for infty)')
    parser.add_argument('-o',
                        '--output',
                        type=str,
                        default='./output.log',
                        help='file to write outputs of the running program')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        help='flag whether to show debug information')

    args = parser.parse_args()
    name = args.name
    ip = args.ip
    port = args.port
    rtm_ip = args.rtm_ip
    rtm_port = args.rtm_port
    timeout = args.timeout
    is_debug = args.debug
    log_file_name = args.output

    DebugLogger.set_base_logger_info()
    if is_debug:
        DebugLogger.set_base_logger_debug()

    input('\n\n\nHit enter to start\n\n')
    run_application_client(name, ip, port, rtm_ip, rtm_port, timeout,
                           log_file_name)

    print(f"execution finished. program output written to '{log_file_name}'")


if __name__ == '__main__':
    main()
