# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

'''
TTPython programs are dataflow graphs, composed of nodes or 'Scheduling Quanta'
(SQs). SQs in the graph are connected by arcs, which generlly correspond to
variable names in the TTPython program

The graph is generated by our ``Compiler`` based pythonic function that has been
given the 'GRAPHify' decorator. The graph also includes additional constructs
like a set of clocks and constraints, which are generally described using 'with'
constructs
'''

from . import DebugLogger

logger = DebugLogger.get_logger('Graph')

class TTGraph():
    '''
    TTGraphs are created as empty graphs, containing only empty instance
    variables that will be filled in during the compilation process.

    The graph contains the set of available base SQ functions, a list of those
    SQs as they are implemented within the graph (including the arc
    connections), a set of clocks, a set of symbols, a set of input or 'trigger'
    arcs, and a set of constraints to apply to SQs (or groups thereof).

    '''

    def __init__(self, library_path=None):
        self.sq_function_dictionary = {}
        self.GRAPHified_function = None
        self.sq_list = []
        self.clock_dictionary = {}
        self.symbol_table = {}   # dict of 'unique-symbol' : TTArc
        # In the main function, record the first arg here.
        # It will be used to trigger CONST()
        self.trigger_arc = None
        self.flattened_constraints = []
        self.graph_name = None
        self.modules_imported = []
        self.library_path = library_path

    def input_arc_dict(self):
        '''
        Generate a dictionary of the input arcs to the graph (i.e., the arcs
        that must be given some initial value at runtime to trigger the rest of
        the graph to start). Those arcs have no source SQ, and must have
        destination SQs
        '''
        arc_dict = {}
        for _, (symbol, arc) in enumerate(self.symbol_table.items()):
            if arc.source_sq is None and arc.dest_sq_list != []:
                arc_dict[symbol] = arc
        return arc_dict

    def output_arc_dict(self):
        '''
        Generate a dictionary of the output arcs in the graph (i.e., the arcs
        that are terminated points in the graph). These arcs have no destination
        SQ, but do have a source.
        '''
        arc_dict = {}
        for _, (symbol, arc) in enumerate(self.symbol_table.items()):
            if arc.source_sq is not None and arc.dest_sq_list == []:
                arc_dict[symbol] = arc
        return arc_dict

    def internal_arc_dict(self):
        '''
        Generate a dictionary of the internal arcs in the graph These arcs have
        a source and at least one destination SQ
        '''
        arc_dict = {}
        for _, (symbol, arc) in enumerate(self.symbol_table.items()):
            if arc.source_sq is not None and arc.dest_sq_list != []:
                arc_dict[symbol] = arc
        return arc_dict

    def json_graph_inputs(self):
        inputs = {}
        for _, (symbol, arc) in enumerate(self.input_arc_dict().items()):
            inputs[symbol] = arc.arc_id()
        return inputs

    def json_graph_outputs(self):
        outputs = {}
        for _, (symbol, arc) in enumerate(self.output_arc_dict().items()):
            outputs[symbol] = arc.arc_id()
        return outputs

    def json_clocks(self):
        j = []
        for _, (_, clockspec) in enumerate(self.clock_dictionary.items()):
            j.append(clockspec.json())
        return j

    def json_port_map(self):
        j = []
        for _, (_, arc) in enumerate(self.symbol_table.items()):
            j.append({'port_name': arc.arc_id(),
                      'type': 'dynamic'})
        return j

    def json(self):
        j = []
        json_sqs = []
        for this_sq in self.sq_list:
            json_sqs.append(this_sq.json())
        j.append({'graph_id': self.graph_name,
                  'graph_inputs': self.json_graph_inputs(),
                  'graph_outputs': self.json_graph_outputs(),
                  'clocks': self.json_clocks(),
                  'SQs': json_sqs,
                  'port_map': self.json_port_map(),
                  'flattened_constraints': [c.json() for c in self.flattened_constraints]
                  })
        return j

    def set_flattened_constraints(self, constraints):
        self.flattened_constraints = constraints
