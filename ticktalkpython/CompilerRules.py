# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
'''
These rules are automatically called as the ast (abstract syntax tree) library
'visits' syntactic structures within the TTPython file to compile.

The ``TTSQ`` constructor is also an especially essential component of the
compilation process!
'''

import ast
import sys
import importlib
import os
import astor  # be sure to pip install this...
import astunparse

from typing import Any, List
from functools import reduce

from .SQ import TTSQ, TTSQContext
from .Stack import TTStack
from .Clock import TTClock
from .Arc import TTArc
from .Error import TTSyntaxError
from .Error import TTCompilerError
from . import Query
from . import DebugLogger
from .Graph import TTGraph
from .FiringRule import TTFiringRuleType

logger = DebugLogger.get_logger('CompilerRules')

package_name = "ticktalkpython"


class TTImportVisitor(ast.NodeVisitor):
    ''''''
    def __init__(self, graph, debug=True):
        self.graph = graph
        self.debug = debug

    def visit_ImportFrom(self, node):
        logger.debug(ast.dump(node))
        if node.module is None:
            substitute_module = node.names[0].name
            if substitute_module in self.graph.modules_imported:
                return
            # logger.debug(f"Substituting {substitute_module} for node.module")
            # logger.debug(f"Importing SQified functions from module {substitute_module}.py")
            import_sqified_functions_from_module(substitute_module, self.graph)
        else:
            if node.module in self.graph.modules_imported:
                return
            # logger.debug(f"Attempting to import SQified functions from module {node.module}.py")
            import_sqified_functions_from_module(node.module, self.graph)

    def visit_FunctionDef(self, node):
        for decorator in node.decorator_list:
            # print(astor.dump_tree(decorator))
            if (isinstance(decorator, type(ast.Name()))
                    and (decorator.id == 'SQify' or decorator.id == 'STREAMify'
                         or decorator.id == "Resampler")):
                # logger.debug(f"SQified function: {node.name}")
                self.graph.sq_function_dictionary[node.name] = node


def get_deps(node):
    _, statements = next(ast.iter_fields(node))

    full_graph = {
        assign.targets[0].id:
        [d.id for d in ast.walk(assign) if isinstance(d, ast.Name)]
        for assign in statements
    }
    # full_graph also contains `range` and `i`. Keep only top levels var
    restricted = {}
    for var in full_graph:
        restricted[var] = [
            d for d in full_graph[var] if d in full_graph and d != var
        ]
    return restricted


# Search for a module but do not load it.
# Return the path if it is found
#
# Follow Python-ish rules for locating a module
#   * "from . import TTComponent":             Look for TTComponent in
#                                              the package's directory
#   * "from .TTComponent import foo":          Look for TTComponent in
#                                              the package's directory
#   * "from packagename import foo":           If packagename==ticktalkpython,
#                                              look for foo in the package's
#                                              directory
#   * "from packagename.component import foo": If packagename==ticktalkpython,
#                                              look for component in the
#                                              package's directory
#


def pathname_for_module(module, library_path=None):
    # Case 1: a file included in this package
    #
    # The absolute path to the package directory is the same
    # as the absolute path to this source file: __file__
    if library_path is None:
        package_path = os.path.dirname(__file__)
    else:
        package_path = library_path

    # Case 1: Implicit package-local ref, e.g., ".Instructions"
    # This does not properly handle .foo.bar
    if module[0] == ".":
        pathname = f"{package_path}/{module[1:]}.py"
        if os.path.exists(pathname):
            return pathname

    split = module.split('.')
    if (len(split) == 2) and (split[0] == package_name):
        # Case 2: Explicit package-local ref, e.g.,
        # "ticktalkpython.Instructions"
        pathname = f"{package_path}/{split[1]}.py"
    else:
        pathname = f"{package_path}/{module}.py"
    logger.debug(f"      |___ Trying {pathname}")
    if os.path.exists(pathname):
        return pathname

    # Try sys.path() prefixes
    if library_path is None:
        for path_prefix in sys.path:
            pathname = f"{path_prefix}/{module}.py"
            logger.debug(f"      |___ Trying {pathname}")
            if os.path.exists(pathname):
                return pathname
        if len(split) > 1:
            logger.debug(f"          {module}: multi-part module")
            package = '.'.join(split[0:len(split) - 1])
            spec = importlib.util.find_spec(split[-1], package=package)
        else:
            spec = importlib.util.find_spec(module)
        if spec is None:
            return None
        else:
            return spec.origin
    else:
        return None


# Enter with the name of a module from an "import" statement.
#
# If found, read the file and generate an AST.
# Visit the AST for SQified functions.
def import_sqified_functions_from_module(module, graph):
    '''
    Find the module and import any SQify or STREAMify-decorated functions
    '''
    logger.debug(f"  |___ {module}: Looking for it")
    module_path = pathname_for_module(module, library_path=graph.library_path)
    if module_path is None:
        logger.debug(f"       {module}: no path to it -- skipping")
        return
    logger.debug(f"       {module}: found it at {module_path}")
    if module_path == 'built-in':
        logger.debug(f"       {module}: built-in module -- skipping")
        return
    with open(module_path, "r", encoding='utf-8') as source:
        graph.modules_imported.append(module)
        try:
            imported_module = ast.parse(source.read())
        except:
            logger.debug(f"       {module}: import failed -- skipping")
            return
        TTImportVisitor(graph).visit(imported_module)
        logger.debug(f"       {module}: import succeeded")


# gets all free variables in an expression
class TTEnvVisitor(ast.NodeVisitor):
    def __init__(self, node):
        self.node = node
        self.has_constants = False
        self.env = self.visit(node)

    def get_env(self):
        return self.env

    def visit_BinOp(self, node: ast.BinOp) -> set:
        lhs = self.visit(node.left)
        rhs = self.visit(node.right)
        return lhs | rhs

    def visit_UnaryOp(self, node: ast.UnaryOp) -> set:
        return self.visit(node.operand)

    def visit_Name(self, node: ast.Name) -> set:
        return {node.id}

    def visit_Call(self, node: ast.Call) -> set:
        # If a func has 0 arguments, it still needs a trigger
        # if len(node.args) == 0:
        #     self.has_constants = True

        return reduce((lambda acc, arg: acc | self.visit(arg)), node.args,
                      set())

    def visit_IfExp(self, node: ast.IfExp) -> set:
        return self.visit(node.test) | self.visit(node.body) | self.visit(
            node.orelse)

    def visit_Compare(self, node: ast.Compare) -> set:
        return reduce((lambda acc, c: acc | self.visit(c)), node.comparators,
                      self.visit(node.left))

    def visit_BoolOp(self, node: ast.BoolOp) -> set:
        return reduce(
            (lambda acc, clause: acc | self.visit(clause), node.values, set()))

    def visit_Constant(self, node: ast.Constant) -> set:
        self.has_constants = True
        return set()

    # backward compatibility with 3.7
    def visit_Num(self, node: ast.Constant) -> List[TTArc]:
        return self.visit_Constant(node)

    def visit_NameConstant(self, node: ast.NameConstant) -> List[TTArc]:
        return self.visit_Constant(node)

    def generic_visit(self, node: ast.AST) -> set:
        logger.debug(f"TTEnvVisitor visited {node}")
        return set()


class Context():
    def __init__(self, trigger_arc: TTArc, sym_table: dict):
        self.trigger_arc = trigger_arc
        self.sym_table = sym_table

    def merge_shadow_sym_table(self, sym_table: dict):
        '''
        Will merge the two sym_tables. A shadow sym table is where the key for
        the arc might not match its symbol in the arc. Will throw an error if
        there is any
        overlap
        '''
        for arc in sym_table.values():
            if arc.symbol in self.sym_table:
                raise TTCompilerError(
                    f"Two symbol tables are overlapping {arc.symbol}!")
            self.sym_table[arc.symbol] = arc


class ShadowEnv():
    def __init__(self, env, has_constants: bool, rename: str):
        self.env = env
        self.ordered_env = list(env)
        self.has_constants = has_constants
        self.rename = rename
        self.trigger_name = "trigger"

    def get_env_list(self):
        return self.get_renamed_env_list("")

    # the normal use of this func is to leave the param rename None
    def get_renamed_env_list(self, rename=None):
        prepended_name = self.rename
        # allow "" to override rename
        if rename is not None:
            prepended_name = rename

        l = []
        # we always want to rename trigger though
        if self.has_constants:
            l = [self.rename + self.trigger_name]
        return [prepended_name + sym for sym in self.ordered_env] + l

    def get_trigger_name(self):
        return self.rename + self.trigger_name


class TTGraphCompilationVisitor(ast.NodeVisitor):
    '''
    A child class of ast.NodeVisitor, which walks through the ast and calls
    specific handlers for each syntactic construct

    :param graph: An instantiated but empty graph, ready to be filled in as the
        AST is walked
    :type graph: TTGraph
    '''
    def __init__(self,
                 graph: TTGraph,
                 debug=False,
                 source=None,
                 pathname=None):
        self.graph = graph
        self.context_stack = TTStack()
        self.debug = debug
        self.scope_info = None
        self.source = [] if source is None else source
        self.pathname = pathname
        self.symbol_counter = 0
        self.sq_counter = 0
        self.func_counter = 0
        self.converting_literal_to_CONST = False
        self.flattened_constraints = []

    def gensym(self):
        new_symbol = f"${self.symbol_counter}"
        self.symbol_counter += 1
        return new_symbol

    def get_uniq_sq_name(self, sq_name):
        name = sq_name + "-" + str(self.sq_counter)
        self.sq_counter += 1
        return name

    def get_uniq_func_name(self, func_name):
        name = func_name + "_" + str(self.func_counter)
        self.func_counter += 1
        return name

    def source_line(self, lineno):
        return self.source[lineno - 1]

    # this stateful function modifies the graph symbol table
    # assigns a name (symbol) for each unassigned arc in the list
    def fix_arc_list_symbols(self, arc_list: List[TTArc]):
        for arc in arc_list:
            if arc.symbol is None:
                arc.symbol = self.gensym()
                self.graph.symbol_table[arc.symbol] = arc

    def n_compare_generator(self, input_arcs, bool_type):
        if type(bool_type) is ast.And:
            func_name = 'ANDN'
            join_op = ' and '
        elif type(bool_type) is ast.Or:
            func_name = 'ORN'
            join_op = ' or '
        else:
            raise TTCompilerError(f"Unfamiliar boolean n-type {bool_type}")

        # params are [b0, b1, ..., bn]
        bool_param_list = ["b" + str(i) for i in range(0, len(input_arcs))]
        # create an meta boolean node, inputs are defined by input_arcs
        bool_py_func_str = "@SQify\ndef " + func_name + "(" + \
            ', '.join(bool_param_list) + "):\n\t return " + join_op.join(bool_param_list)

        bool_call_node = ast.Call(ast.Name(id=func_name), input_arcs, [])
        # returns a module with a single function def. We only want the func def
        bool_py_func = ast.parse(bool_py_func_str).body[0]

        sq_name = self.get_uniq_sq_name(func_name)

        sq = self.generate_sq(bool_call_node, bool_py_func, sq_name,
                              input_arcs)

        return sq.get_output_arcs()

    def generate_sq(self,
                    node: ast.Call,
                    func: ast.FunctionDef,
                    name: str,
                    input_arcs: List[TTArc],
                    is_singleton=False,
                    input_ctrl_arc=None,
                    firing_rule: TTFiringRuleType=None,
                    num_output_arcs=1) -> TTSQ:
        '''
        Preps the input arcs, creates SQ, and adds it to the sq_list
        '''
        firing_rule_type = firing_rule
        if not firing_rule_type:
            firing_rule_type = self.determine_firing_rule(input_arcs, func)

        # SQ uses these arcs, so label them
        self.fix_arc_list_symbols(input_arcs)

        context = self.context_stack.tos()
        sq = TTSQ(node,
                  func,
                  context,
                  name,
                  firing_rule_type,
                  input_arcs,
                  num_output_arcs,
                  input_control_arc=input_ctrl_arc,
                  is_singleton=is_singleton,
                  clock_dict=self.graph.clock_dictionary)
        self.graph.sq_list.append(sq)

        return sq

    def generate_merge_sq(self, merge_input_arcs: List[TTArc]):
        merge_func_name = "MERGE"

        # Instructions.py should have been loaded by now
        try:
            merge_py_func = self.graph.sq_function_dictionary[merge_func_name]
        except:
            raise TTCompilerError("Instructions.py failed to load, "
                                  f"could not find the {merge_func_name} SQ")

        merge_sq_name = self.get_uniq_sq_name(merge_func_name)

        merge_call_node = ast.Call(ast.Name(id=merge_func_name),
                                   merge_input_arcs, [])

        merge_sq = self.generate_sq(merge_call_node,
                                    merge_py_func,
                                    merge_sq_name,
                                    merge_input_arcs,
                                    firing_rule=TTFiringRuleType.Immediate)

        return merge_sq.output_arcs

    def generate_branch_sq(self, input_env: List[TTArc], has_constants: bool):
        '''
        Generates a string representing a Python if_else
        '''
        pass

    def determine_firing_rule(self, input_arcs: List[TTArc],
                              func: ast.FunctionDef) -> TTFiringRuleType:
        function_source = astunparse.unparse(func)

        # the default firing rule
        firing_rule_type = TTFiringRuleType.Timed

        # streaming designation should propagate; check by tracing to the
        # input arc, assuming the graph is constructed s.t. every new SQ
        # receives input from SQs that have already been created (and their
        # arcs defined)
        is_streaming = any([
            input_arc.source_sq.is_streaming for input_arc in input_arcs
            if input_arc.source_sq is not None
        ])

        # if this is decorated with 'STREAMify', then it should be expected
        # to retrigger itself periodically such that it generates a stream
        # of data. The parameters (e.g., period, phase) will be extracted
        # while analyzing keyword arguemnts
        if any(
            [decorator.id == 'STREAMify'
             for decorator in func.decorator_list]):
            firing_rule_type = TTFiringRuleType.TimedRetrigger

        # If a node has global sq_state and is not streamify, it must have the
        # SequentialRetrigger rule
        elif "global sq_state" in function_source and is_streaming:
            firing_rule_type = TTFiringRuleType.SequentialRetrigger

        return firing_rule_type


    def scrape_kwarg(self, s, node: ast.Call):
        '''
        Searches for the value of key s in kwargs.
        Note that the node needs keywords
        '''
        name = node.func.id
        check = [
            kwarg.value for kwarg in
            [kw for kw in node.keywords if kw.arg == s]
        ]

        if len(check) != 1:
            raise TTSyntaxError(f"{name} requires the {s} keyword",
                                node.lineno, self.source_line(node.lineno),
                                self.pathname)

        return check[0]


    # TODO: We'd rather pass around contexts as opposed to using a global
    # TODO: reassigned context. This is a major overhaul of function
    # TODO: signatures though
    def replace_context(self, ctxt: Context):
        '''
        Returns the old context and replaces the current context with the one
        given
        '''
        c = Context(self.graph.trigger_arc, self.graph.symbol_table)
        self.graph.symbol_table = ctxt.sym_table
        self.graph.trigger_arc = ctxt.trigger_arc

        return c


    def compile_with_shadow_table(self, node, syms: List[str],
                                  rename_arcs: List[str], arcs: List[TTArc],
                                  has_trigger: bool):
        '''
        Compiles node with a shadow table. The table is created by the given
        syms and output_arcs.
        '''
        assert len(syms) == len(rename_arcs)
        assert len(syms) == len(arcs)
        shadow_symbol_table = dict(zip(syms, arcs))

        # save the original trigger and prep new triggers if necessary
        # trigger arcs are appended to the end of the arc list
        shadow_trigger = self.graph.trigger_arc
        if has_trigger:
            shadow_trigger = arcs[-1]

        # visit both branches, and replace the symbol table/trigger
        # respectively
        orig_context = self.replace_context(
            Context(shadow_trigger, shadow_symbol_table))
        compiled_arcs = self.visit(node)

        # rename all arcs
        for rename, arc in zip(rename_arcs, arcs):
            arc.symbol = rename
        renamed_sym_table = dict(zip(rename_arcs, arcs))

        # need to put back then/else shadow tables into the real table
        # rename each arc name to not coincide with other arcs
        # TODO: this is necessary because mapping uses the symbol_table
        # TODO: maybe mapping should not rely on the symbol table?
        orig_context.merge_shadow_sym_table(renamed_sym_table)

        self.replace_context(orig_context)

        return compiled_arcs

    ###################### Visitor functions #####################

    def visit_ImportFrom(self, node):
        logger.debug(
            f"___ Attempting to import SQ definitions from module {node.module}"
        )
        # Checks whole module rather than just the specified alias
        import_sqified_functions_from_module(node.module, self.graph)
        return node

    def visit_Import(self, node):
        for alias in node.names:
            logger.debug(
                f"___ Attempting to import SQ definitions from module {alias.name}"
            )
            import_sqified_functions_from_module(alias.name, self.graph)
        return node

    def visit_FunctionDef(self, node):
        # if self.debug:
        logger.debug(f"*** function: {node.name}")
        for decorator in node.decorator_list:
            logger.debug(f"*** decorator: {decorator.id}")
            if decorator.id == 'SQify':
                self.graph.sq_function_dictionary[node.name] = node
            if decorator.id == 'STREAMify':
                self.graph.sq_function_dictionary[node.name] = node

            elif decorator.id == 'GRAPHify':
                self.graph.GRAPHified_function = node
                self.graph.graph_name = node.name
                # Create distinguished arcs in the symbol table for the graph's inputs
                # print(astor.dump_tree(node))
                for arg in node.args.args:
                    name = arg.arg
                    arc = TTArc(None, name)  # SQ: None
                    self.graph.symbol_table[name] = arc
                    if self.graph.trigger_arc is None:
                        # We will use this input to trigger implicitly-defined
                        # constants. But we REALLY need to expand the
                        # timestamp...
                        self.graph.trigger_arc = arc
                # Recursively descend into the body
                self.context_stack.push(TTSQContext("root"))
                for child in node.body:
                    logger.log(5, child)
                    self.visit(child)
                self.context_stack.pop()

    def visit_With(self, node):
        # Handle the ast within a 'with' specifier, which are used in TTPython
        # to specify things like clocks or mapping constraints Clone the
        # current context as a starting point
        new_context = TTSQContext(base_context=self.context_stack.tos())
        # Iterate over the context modifications
        for item in node.items:
            # **TEMPORARY**  -- instead, do a pass on the AST first to
            # build the clock tree.  Then insert the TTClock object here
            # instead of the name.
            if not isinstance(item.context_expr, type(ast.Call())):
                err = TTSyntaxError('Illegal context specifier', item.lineno)
                err.source = self.source_line(item.lineno)
                err.pathname = self.pathname
                raise err
            # print(astor.dump_tree(item))
            # withitem(context_expr=Call(func=Attribute(value=Name(id='TTClock'),
            #                                           attr='root'),
            #                            args=[],
            #                            keywords=[]),
            #          optional_vars=Name(id='CLOCK'))
            # Could be TTClock(...)
            #   function_id = item.context_expr.func.id
            # or it could be TTClock.root()
            #   function_id = item.context_expr.func.value.id

            root_clock = False
            # Hackish way to do this:
            try:
                function_id = item.context_expr.func.id
            except:
                function_id = item.context_expr.func.value.id
                root_clock = True

            new_context.name = function_id
            if function_id == "TTClock":
                clock_var_name = item.optional_vars.id  # e.g., 'CLOCK'
                if root_clock:
                    # assume the print name as 'ROOT' so it doesn't have to be specified
                    clock_print_name = 'ROOT'
                else:
                    # e.g., 'local_root'
                    clock_print_name = item.context_expr.args[0].s
                new_context.name = f'with TTClock({clock_print_name})'

                if clock_var_name in self.graph.clock_dictionary.keys():
                    err = TTSyntaxError(
                        f"Clock name {clock_var_name} is being re-defined here",
                        item.lineno)
                    err.source = self.source_line(item.lineno)
                    err.pathname = self.pathname
                    raise err

                n_args = 1 if root_clock else len(item.context_expr.args)
                if n_args == 1:
                    # if there are no other arugments, this must be the root cock
                    new_clock = TTClock.root()
                    new_clock.name = clock_print_name
                elif n_args == 4:
                    # there should be 4 args for non-root clocks: name,
                    # parent, period, epoch
                    parent_clock = item.context_expr.args[1].id
                    try:
                        self.graph.clock_dictionary[parent_clock]
                    except KeyError:
                        raise TTSyntaxError(
                            f"Attemped to create a clock {clock_print_name} "
                            f"for a parent '{parent_clock}' that has not been specified",
                            node.lineno)
                    # **Check** should only be a Num node
                    period = item.context_expr.args[2].n
                    # **Check** should only be a Num node
                    epoch = item.context_expr.args[3].n

                    # search for the parent clock based on variable name
                    parent_clock = self.graph.clock_dictionary[
                        item.context_expr.args[1].id]
                    new_clock = TTClock(clock_print_name, parent_clock, period,
                                        epoch)
                else:
                    err = TTSyntaxError("Incorrect arglist for clock spec",
                                        item.lineno)
                    err.source = self.source_line(item.lineno)
                    err.pathname = self.pathname
                    raise err

                self.graph.clock_dictionary[clock_var_name] = new_clock
                new_context.clock = new_clock
            elif function_id == "TTPlanB":
                new_context.name = 'with TTPlanB'
                # **Placeholder**. Not implemented
            elif function_id == "TTDeadline":
                new_context.name = 'with TTDeadline'
                # **Placeholder**. Not implemented
            elif function_id == "TTConstraint":
                # TTQuery to apply mapping contraints to any SQs that appear
                # within this block
                new_context.name = 'with TTConstraint'
                kwargs = item.context_expr.keywords
                # we assume the 'components' kwarg will point to a list
                component_list_search = [
                    k.value.elts for k in kwargs if k.arg == 'components'
                ]
                component_list = component_list_search[0] if 0 < len(
                    component_list_search) else []

                # if expr required for 3.7 ast parsing
                ens_name_list = (lambda l: l if len(l) == 1 else [])([
                    Query.TTQCEnsembleName(k.value.s if isinstance(
                        k.value, ast.Str) else k.value.value) for k in kwargs
                    if k.arg == 'name'
                ])

                # assumes args are string constants
                # if expr required for 3.7 ast parsing
                name_query_list = [
                    Query.TTQCComponentName(
                        c.s if isinstance(c, ast.Str) else c.value)
                    for c in component_list
                ]

                new_context.constraints = name_query_list + ens_name_list
                self.flattened_constraints.extend(new_context.constraints)
            else:
                err = TTSyntaxError("Illegal context specifier", node.lineno)
                err.source = self.source_line(node.lineno)
                err.pathname = self.pathname
                raise err
        # if self.debug:
        #    print (f"*** New context {new_context}")       # Name of the context function
        self.context_stack.push(new_context)

        # Recursively descend into the body
        for child in node.body:  # ast.iter_child_nodes(node.body):
            logger.debug('Child of functionDef visit: %s' % child)
            self.visit(child)
        self.context_stack.pop()
        # if self.debug:
        #    logger.debug(f"*** Popped to context {self.context_stack.tos()}")

    def visit_Assign(self, node):
        # Logic:
        #
        # Process the right hand side of the assigmnet (an expression).  The
        # value returned will be a TTArc with an assigned source SQ.  Add an
        # entry to the symbol table that links the left hand symbol to this
        # TTArc.
        #
        # print(astor.dump_tree(node))
        assignment_list = node.targets
        # In TTPython, we only allow assignment to a single identifier in a
        # given assignment statement.
        if len(assignment_list) > 1:
            err = TTSyntaxError(
                'Only one identifier is allowed on the left hand side of an assignment',
                node.lineno)
            err.source = self.source_line(node.lineno)
            err.pathname = self.pathname
            raise err
        raw_symbol = assignment_list[0].id

        # Symbols may be re-used, and the usage in a scope may shadow a
        # different usage in an outer scope. Disallow re-definititions within
        # a scope, and re-name shadowing symbols to dis-ambiguate them from
        # shadowed definitions in outer scopes

        # **Temporary**
        unique_symbol = raw_symbol
        if unique_symbol in self.graph.symbol_table.keys():
            err = TTSyntaxError('Multiple assignments to the same symbol',
                                node.lineno)
            err.source = self.source_line(node.lineno)
            err.pathname = self.pathname
            raise err
        # Expecting an Expr
        arc_list = self.visit(node.value)
        if 1 < len(arc_list):
            err = TTSyntaxError('lhs expression returns more than 1 value!',
                                node.lineno)
            err.source = self.source_line(node.lineno)
            err.pathname = self.pathname
            raise err

        # has this been already used? if so, we can just rename it
        # TODO: this may have been causing some bugs earlier with the
        # TODO: inclusion of the if/else Check this
        if arc_list[0].symbol is not None:
            logger.warning(
                f"rewriting symbol {arc_list[0].symbol} to {unique_symbol}")
            del self.graph.symbol_table[arc_list[0].symbol]

        arc_list[0].symbol = unique_symbol
        self.graph.symbol_table[unique_symbol] = arc_list[0]

    def visit_Return(self, node):
        # Logic:
        #
        # Process the right hand side of the assignment (an expression).
        # The value returned will be a TTArc with an assigned source SQ.
        # print(astor.dump_tree(node))

        arc_list = self.visit(node.value)  # Expecting an Expr
        self.fix_arc_list_symbols(arc_list)

    def visit_UnaryOp(self, node):
        # print(astor.dump_tree(node))
        if isinstance(node.op, ast.USub):
            func_string = 'NEG'
        elif isinstance(node.op, ast.Not):
            func_string = 'NOT'
            pass
        else:
            err = TTSyntaxError('Unrecognized unary operation', node.lineno)
            err.source = self.source_line(node.lineno)
            err.pathname = self.pathname
            raise err
        func = ast.Name(id=func_string)
        arc_arg_list = self.visit(node.operand)
        if len(arc_arg_list) != 1:
            raise TTSyntaxError('Unary operation received more than 1 expr',
                                node.lineno)

        self.fix_arc_list_symbols(arc_arg_list)
        arc_arg = arc_arg_list[0]
        args = [ast.Name(id=arc_arg.symbol)]
        call_node = ast.Call(func, args, [])
        # Process the new Call node -- it should return an Arc -- we return
        # that Arc in turn
        return self.visit_Call(call_node)

    def visit_BinOp(self, node):
        # In:  BinOp(left=BinOp(left=Name(id='a'), op=Add, right=Name(id='b')),
        #           op=Mult,
        #           right=BinOp(left=Name(id='a'), op=Sub, right=Name(id='b'))))
        # print(astor.dump_tree(node))
        if isinstance(node.op, type(ast.Add())):
            func_string = 'ADD'
        elif isinstance(node.op, type(ast.Sub())):
            func_string = 'SUB'
        elif isinstance(node.op, type(ast.Mult())):
            func_string = 'MULT'
        elif isinstance(node.op, type(ast.Div())):
            func_string = 'DIV'
        else:
            raise TTSyntaxError('Unrecognized binary operation', node.lineno,
                                self.source_line(node.lineno), self.pathname)
        func = ast.Name(id=func_string)

        # Visit the left and right nodes -- each should return a TTArc.
        # Each TTArc may be a result of the node being a Name (easy),
        # but if the node a Call or another BinOp, then the TTArc
        # has no programmer-specified symbol with which the arc
        # is associated.  So we make one up.
        arc_left_list = self.visit(node.left)
        arc_right_list = self.visit(node.right)

        if len(arc_left_list) != 1:
            raise TTSyntaxError(
                'Binary operation received more than 1 expr on lhs',
                node.lineno)
        if len(arc_right_list) != 1:
            raise TTSyntaxError(
                'Binary operation received more than 1 expr on rhs',
                node.lineno)

        self.fix_arc_list_symbols(arc_left_list)
        self.fix_arc_list_symbols(arc_right_list)
        arc_left = arc_left_list[0]
        arc_right = arc_right_list[0]
        args = [ast.Name(id=arc_left.symbol), ast.Name(id=arc_right.symbol)]
        call_node = ast.Call(func, args, [])
        # Process the new Call node -- it should return an Arc -- we return
        # that Arc in turn
        return self.visit_Call(call_node)

    def visit_BoolOp(self, node: ast.BoolOp) -> List[TTArc]:
        bool_input_arcs = [
            input_arc
            for arc_list in [self.visit(clause) for clause in node.values]
            for input_arc in arc_list
        ]

        return self.n_compare_generator(bool_input_arcs, node.op)

    def visit_Compare(self, node: ast.Compare) -> List[TTArc]:
        arc_left_comp_list = self.visit(node.left)
        arc_comp_list = [
            input_arc
            for arc_list in [self.visit(comp) for comp in node.comparators]
            for input_arc in arc_list
        ]
        arcs = arc_left_comp_list + arc_comp_list

        self.fix_arc_list_symbols(arcs)

        arc_and_list = []
        for i in range(0, len(arcs) - 1):
            left_arc = arcs[i]
            right_arc = arcs[i + 1]

            if isinstance(node.ops[i], type(ast.Eq())):
                func_string = 'EQ'
            elif isinstance(node.ops[i], type(ast.NotEq())):
                func_string = 'NEQ'
            elif isinstance(node.ops[i], type(ast.Lt())):
                func_string = 'LT'
            elif isinstance(node.ops[i], type(ast.LtE())):
                func_string = 'LTE'
            elif isinstance(node.ops[i], type(ast.Gt())):
                func_string = 'GT'
            elif isinstance(node.ops[i], type(ast.GtE())):
                func_string = 'GTE'
            else:
                raise TTSyntaxError(
                    f'Compare operator {node.ops[i]} not implemented',
                    node.lineno)

            func = ast.Name(id=func_string)
            args = [
                ast.Name(id=left_arc.symbol),
                ast.Name(id=right_arc.symbol)
            ]
            call_node = ast.Call(func, args, [])
            # Process the new Call node -- it should return an Arc -- we
            # return that Arc in turn
            arc_and_list.extend(self.visit_Call(call_node))

        return self.n_compare_generator(arc_and_list, ast.And())

    # Create an SQ for this call and a TTArc for its output.
    # Return TTArcs ina list
    def visit_Call(self, node):
        # Ignore top-level calls in the source file (e.g. print(main(...)))
        # "top-level" is recognizable because the context has not yet been defined.
        if self.context_stack.tos() is None:
            return
        # Each call gives rise to a new SQ.  Allocate it (and with it, the TTArc
        # at its output).
        #
        # Each call consumes one or more inputs -- find the corresponding TTArcs, and
        # annotate them with the SQ and port of this SQ

        # Create the SQ for this Call
        func_name = node.func.id
        # Recursively visit the args and collect the TTArcs that are returned
        input_arcs = []

        logger.debug(astor.dump_tree(node))

        # TODO 1: need to consolidate this with TTFinishByOtherwise
        if func_name == 'TTSingleRunTimeout':
            timeout_node = self.scrape_kwarg("TTTimeout", node)
            timeout = timeout_node.n if isinstance(timeout_node,
                                                   ast.Num) else timeout_node.value
            if not type(timeout) is int:
                raise TTSyntaxError(f"Timeout must be an integer.",
                                    node.lineno, self.source_line(node.lineno),
                                    self.pathname)

            srun_sq_name = self.get_uniq_sq_name("SINGLETON")
            srun_func_name = srun_sq_name.replace("-", "_")

            free_vars_visitor = TTEnvVisitor(node)

            srun_shadow_env = ShadowEnv(free_vars_visitor.get_env(),
                                        free_vars_visitor.has_constants,
                                        srun_func_name + "_")
            srun_env = srun_shadow_env.get_env_list()

            # * Step 2:
            # add outputs to triggers
            srun_py_func_hdr = ""
            if free_vars_visitor.has_constants:
                srun_py_func_hdr = ("\t" + srun_shadow_env.get_trigger_name() +
                                " = 0xDEADBEEF\n")

            param_list = ['TTLock'] + list(free_vars_visitor.get_env())
            srun_py_func_str = ("@SQify\ndef " + srun_func_name + '(' +
                                ', '.join(param_list) + "):\n" +
                                srun_py_func_hdr + '\treturn ' +
                                ', '.join(param_list[1:]))


            srun_py_func = ast.parse(srun_py_func_str).body[0]

            # NOTE: get a separated trigger
            id_func = ast.Name(id='NOT')
            id_args = [ast.Name(id=self.graph.trigger_arc.symbol)]
            # make this node the keyword value of the id() call
            id_call_node = ast.Call(id_func, id_args, [])
            id_arc_list = self.visit_Call(id_call_node)


            # lookup the SQs for each free_var
            srun_circ_arc = id_arc_list[0]
            srun_input_arcs = [srun_circ_arc] + [
                arc for arc_list in [
                    self.visit_Name(ast.Name(id=p))
                    for p in free_vars_visitor.get_env()
                ] for arc in arc_list
            ]

            srun_call_node = ast.Call(ast.Name(id=srun_func_name), srun_input_arcs, [])

            srun_sq = self.generate_sq(srun_call_node,
                                    srun_py_func,
                                    srun_sq_name,
                                    srun_input_arcs,
                                    is_singleton=True,
                                    num_output_arcs=len(srun_env))
            srun_circ_arc.source_sq = srun_sq

            # * Step 3: compile the associated single-run graph
            srun_output_arcs = srun_sq.get_output_arcs()

            for name, srun_output_arc in zip(srun_env, srun_output_arcs):
                srun_output_arc.symbol = name

            # TODO: change hardcoded compile of first arg
            srun_exp_arcs = self.compile_with_shadow_table(
                node.args[0], srun_env, srun_shadow_env.get_renamed_env_list(),
                srun_output_arcs, free_vars_visitor.has_constants)

            # * Step 4: feed back into the parent SQ
            if len(srun_exp_arcs) != 1:
                raise TTSyntaxError(
                    f'The expression in TTSingleRunTimeout must '
                    'have a single output.', node.lineno,
                    self.source_line(node.lineno), self.pathname)

            # TODO: clean up, this makes the TIME_DELAY
            self.fix_arc_list_symbols(srun_exp_arcs)

            delay_func = ast.Name(id='ADD_TIME_DELAY')
            delay_args = [ast.Name(id=arc.symbol) for arc in srun_exp_arcs]
            # make this node the keyword value of the TIME_DELAY() call
            delay_kwarg = ast.keyword(arg='delay', value=ast.Constant(value=timeout))
            delay_keywords = [delay_kwarg]
            delay_call_node = ast.Call(delay_func, delay_args, delay_keywords)
            delay_arc_list = self.visit_Call(delay_call_node)

            # TODO: clean up, this makes the timeout
            self.fix_arc_list_symbols(delay_arc_list)

            timeout_func = ast.Name(id='SET_TIMEOUT')
            timeout_args = [ast.Name(id=arc.symbol) for arc in delay_arc_list]
            # make this node the keyword value of the TIME_DELAY() call
            clock_kwarg = ast.keyword(arg='TTClock', value=ast.Name(id='root_clock'))
            timeout_keywords = [clock_kwarg]
            timeout_call_node = ast.Call(timeout_func, timeout_args, timeout_keywords)
            timeout_arc_list = self.visit_Call(timeout_call_node)

            # TODO: need to reconsider compilation return objects
            circ_sq = None
            for sq in self.graph.sq_list:
                if timeout_arc_list == sq.output_arcs:
                    circ_sq = sq

            if not circ_sq:
                raise TTCompilerError("Couldn't find the sq I just made.")

            circ_sq.output_arcs = [srun_circ_arc]

            return srun_exp_arcs

        # add triggers for void functions
        if len(node.args) == 0:
            logger.debug(f"node has no arguments!")
            input_arcs = [self.graph.trigger_arc]

        for arg in node.args:
            arc_list = self.visit(arg)
            if isinstance(arc_list, List) and isinstance(arc_list[0], TTArc):
                arc = arc_list[0]
                input_arcs.append(arc)

        # TODO 2: move this up, can't visit all the args beforehand if you
        # shadow table (because visiting beforehand defeats the purpose of the
        # shadow table)
        # special translation for TTFinishByOtherwise
        if func_name == 'TTFinishByOtherwise':
            if len(node.args) != 1:
                raise TTSyntaxError(
                    f"TTFinishByOtherwise {func_name} has too many parameters",
                    node.lineno, self.source_line(node.lineno), self.pathname)

            planB = self.scrape_kwarg("TTPlanB", node)
            will_ret = self.scrape_kwarg("TTWillContinue", node)
            time_deadline = self.scrape_kwarg("TTTimeDeadline", node)

            # need to relax ast.Constant for backwards compatibility of ast
            # generation
            if type(will_ret.value) is not bool:
                raise TTSyntaxError(
                    f"TTFinishByOtherwise: TTWillContinue requires a boolean value",
                    node.lineno, self.source_line(node.lineno), self.pathname)

            time_control_arc = self.visit(time_deadline)[0]

            if len(input_arcs) != 1:
                raise TTSyntaxError(
                    "TTFinishByOtherwise only can operate on one output",
                    node.lineno, self.source_line(node.lineno), self.pathname)

            deadline_func_name = 'DEADLINE'
            deadline_sq_name = self.get_uniq_sq_name(deadline_func_name)

            # Instructions.py should have been loaded by now
            try:
                deadline_func = self.graph.sq_function_dictionary[
                    deadline_func_name]
            except:
                raise TTCompilerError(
                    "Instructions.py failed to load, "
                    f"could not find the {deadline_func_name} SQ")

            deadline_node = ast.Call(ast.Name(id=deadline_func_name),
                                     input_arcs, [])

            self.fix_arc_list_symbols(input_arcs)
            self.fix_arc_list_symbols([time_control_arc])
            deadline_sq = self.generate_sq(
                deadline_node,
                deadline_func,
                deadline_sq_name,
                input_arcs,
                input_ctrl_arc=time_control_arc,
                firing_rule=TTFiringRuleType.Deadline,
                num_output_arcs=2)

            # TODO: figure out if callBack is a constant, func call, lambda, etc.
            # TODO: need to include shadow table to also link planB to the
            # TODO: deadline sq output of the
            orig_symbol_table = self.graph.symbol_table
            orig_trigger = self.graph.trigger_arc

            data_arc, deadline_trigger = deadline_sq.get_output_arcs()
            # TODO/FIXME: This should not be fixed automatically
            # Need to actually shadow compile like you do for srun and if/else
            self.fix_arc_list_symbols([deadline_trigger])

            self.graph.trigger_arc = deadline_trigger

            try:
                planB_arcs = self.visit_Call(planB)
            except TTCompilerError as e:
                logger.error(
                    "must be a function planB with TTFinishByOtherwise for now"
                )
                raise e

            self.graph.symbol_table = orig_symbol_table
            orig_symbol_table[deadline_sq_name + "_trigger"] = deadline_trigger
            self.graph.trigger_arc = orig_trigger

            if will_ret.value:  # create a merge SQ like an if/else branch
                if len(planB_arcs) != 1:
                    raise TTSyntaxError(
                        f"Plan B for {func_name} does not "
                        "have a single return value", node.lineno,
                        self.source_line(node.lineno), self.pathname)

                merge_input_arcs = [data_arc] + planB_arcs
                output_arcs = self.generate_merge_sq(merge_input_arcs)
            else:
                output_arcs = [data_arc]

            return output_arcs

        try:
            sqified_function = self.graph.sq_function_dictionary[func_name]
        except KeyError as exc:
            logger.error(f"Undefined SQ: {func_name}")
            logger.error(self.graph.sq_function_dictionary)
            raise TTCompilerError(
                "UndefinedSQ",
                f"SQified function {func_name} was not found") from exc
        if sqified_function:
            # ensure the SQ has a unique name by appending a counter
            sq_name = self.get_uniq_sq_name(func_name)

            sq = self.generate_sq(node, sqified_function, sq_name, input_arcs)

            return sq.get_output_arcs()
        else:
            err = TTSyntaxError(f"SQified function {node.name} is not defined",
                                node.lineno)
            err.source = self.source_line(node.lineno)
            err.pathname = self.pathname
            raise err

    def visit_IfExp(self, node) -> List[TTArc]:
        # Expression(
        # body=IfExp(
        #     test=Name(id='b', ctx=Load()),
        #     body=Name(id='a', ctx=Load()),
        #     orelse=Name(id='c', ctx=Load())))
        # The if expr has 4 stages of compilation: test, if SQ, then and
        # else branch, and merge SQ
        # * Stage 1: test
        arc_check_exp_list = self.visit(node.test)

        # * Stage 2: if SQ
        func_name = "IF"
        if_sq_name = self.get_uniq_sq_name(func_name)
        # vars are named are named uniquely by the sq_name, but hyphens aren't
        # supported in Python var names
        if_func_name = if_sq_name.replace("-", "_")

        # find all free vars in the then and else expressions
        then_visitor = TTEnvVisitor(node.body)
        else_visitor = TTEnvVisitor(node.orelse)

        then_shadow_env = ShadowEnv(then_visitor.get_env(),
                                    then_visitor.has_constants,
                                    if_func_name + "_then_")
        else_shadow_env = ShadowEnv(else_visitor.get_env(),
                                    else_visitor.has_constants,
                                    if_func_name + "_else_")
        then_env = then_shadow_env.get_env_list()
        else_env = else_shadow_env.get_env_list()

        free_vars = list(then_visitor.get_env() | else_visitor.get_env())

        param_list = ["_check"] + free_vars

        then_param_list = ', '.join(then_env) + ', TTEmpty()' * len(else_env)
        else_param_list = 'TTEmpty(), ' * len(then_env) + ', '.join(else_env)

        then_branch_text = ("\tif (_check):\n\t\treturn " + then_param_list +
                            "\n")
        else_branch_text = "\telse:\n\t\treturn " + else_param_list

        if_py_func_hdr = "\tfrom ticktalkpython.Empty import TTEmpty\n"
        # add outputs to triggers
        if then_visitor.has_constants:
            if_py_func_hdr += ("\t" + then_shadow_env.get_trigger_name() +
                               " = None\n")
        if else_visitor.has_constants:
            if_py_func_hdr += ("\t" + else_shadow_env.get_trigger_name() +
                               " = None\n")

        if_py_func_str = ("@SQify\ndef " + if_func_name + "(" +
                          ', '.join(param_list) + "):\n" + if_py_func_hdr +
                          then_branch_text + else_branch_text)
        if_py_func = ast.parse(if_py_func_str).body[0]

        # lookup the SQs for each free_var
        if_input_arcs = arc_check_exp_list + [
            arc for arc_list in
            [self.visit_Name(ast.Name(id=p)) for p in free_vars]
            for arc in arc_list
        ]

        if_call_node = ast.Call(ast.Name(id=if_func_name), if_input_arcs, [])

        if_sq = self.generate_sq(if_call_node,
                                 if_py_func,
                                 if_sq_name,
                                 if_input_arcs,
                                 num_output_arcs=len(then_env) + len(else_env))

        # * Step 3: then and else branches
        if_output_arcs = if_sq.get_output_arcs()

        for name, if_output_arc in zip(then_env + else_env, if_output_arcs):
            if_output_arc.symbol = name

        then_output_arcs = if_output_arcs[:len(then_env)]
        else_output_arcs = if_output_arcs[len(then_env):]

        then_exp_arcs = self.compile_with_shadow_table(
            node.body, then_env, then_shadow_env.get_renamed_env_list(),
            then_output_arcs, then_visitor.has_constants)
        else_exp_arcs = self.compile_with_shadow_table(
            node.orelse, else_shadow_env.get_renamed_env_list(), else_env,
            else_output_arcs, else_visitor.has_constants)

        # technically the merge SQ is part of the shadow compilation for the
        # if node. However, the compilation for the merge SQ does not
        # introduce new symbols and does not use the trigger, so we may
        # compile them independently
        # * Step 4: merge SQ
        # create the join node to merge the if and else branches
        merge_input_arcs = then_exp_arcs + else_exp_arcs
        merge_output_arcs = self.generate_merge_sq(merge_input_arcs)

        return merge_output_arcs


    def visit_Name(self, node) -> List[TTArc]:
        # Look up the name using scope resolution rules
        # For the disambiguated version of the name, find the corresponding
        # arc and return it

        # **In Progress**
        #  What if this is not something specified in the 'with' context?

        try:
            if node.id in self.graph.symbol_table:
                return [self.graph.symbol_table[node.id]]
            elif node.id in self.graph.clock_dictionary:
                return [self.graph.clock_dictionary[node.id]]
            else:
                raise KeyError(node.id)

        except KeyError:
            raise

    def visit_Constant(self, node: ast.Constant) -> List[TTArc]:
        # print(astor.dump_tree(node))

        # A way to handle literal constants in the program -- convert them to
        # CONST() calls
        func = ast.Name(id='CONST')
        # Grab the graph's first input as the trigger
        trigger_arg = ast.Name(id=self.graph.trigger_arc.symbol)
        # make this node the keyword value of the CONST() call
        const_kwarg = ast.keyword(arg='const', value=node)
        args = [trigger_arg]
        keywords = [const_kwarg]
        call_node = ast.Call(func, args, keywords)
        result_arc_list = self.visit_Call(call_node)

        return result_arc_list

    def visit_Module(self, node: ast.Module) -> Any:
        return super().generic_visit(node)

    # 3.7 compatibility
    def visit_Num(self, node) -> List[TTArc]:
        new_node = ast.Constant(node.n)
        return self.visit_Constant(new_node)

    def visit_Str(self, node) -> List[TTArc]:
        new_node = ast.Constant(node.s)
        return self.visit_Constant(new_node)

    def visit_NameConstant(self, node: ast.NameConstant) -> List[TTArc]:
        return self.visit_Constant(node)

    def generic_visit(self, node):
        # print(astor.dump_tree(node))
        err = TTSyntaxError(
            f'This structure ({type(node)}) is not currently supported in TTPython',
            node.lineno, self.source_line(node.lineno), self.pathname)
        raise err
