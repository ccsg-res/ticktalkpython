# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# Deadlines are always with reference to a specific clock and are
# expressed in terms of ticks of that clock
'''
Expression Deadline handling for TTPyton
'''


def TTFinishByOtherwise(self, data_value, TTTimeDeadline, TTPlanB,
                        TTWillContinue):
    '''
    ``TTFinishByOtherwise`` allows for explicit deadline checking associated
    with other tokens. The actual implementation is covered at the dataflow
    graph implementation, hence the empty body.

    This function can only be called in an annotated ``@GRAPHify`` function.

    :param data_value: the token to be used to check if the deadline has been
        met

    :type data_value: TTToken

    :param TTTimeDeadline: the token responsible for setting the deadline. The
        stop_tick time of the token is used as the deadline

    :type TTTimeDeadline: TTToken

    :param TTPlanB: When a deadline is triggered, the expression within Plan B
        will be run. Currently, the value is assumed to be a function call.
        In future releases, this will be a generic expression.

    :type TTPlanB: Python expression

    :param TTWillContinue: this specifies whether a value will be propagated
        after the Plan B expression runs. Setting this to False will stop
        any further token generation, which effectively stops any
        downstream nodes from firing in the same iteration

    :type TTWillContinue: bool
    '''
    pass
