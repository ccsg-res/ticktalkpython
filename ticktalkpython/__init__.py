'''
TickTalkPython is a language, compiler and runtime for programming
distributed, federated, time-sensitive systems.
'''

import sys
import os
