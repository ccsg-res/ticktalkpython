# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

'''
Stream Merge Test
'''

import re
import traceback
import pickle
import time
import simpy
from matplotlib import pyplot as plt

# from ticktalkpython.Compiler import draw_graph
from ticktalkpython import DebugLogger
from ticktalkpython.RuntimeManager import TTRuntimeManager
from ticktalkpython.RuntimeManager import TTRuntimeManagerSim
from ticktalkpython.RuntimeManager import TTRuntimeManagerPhysical
from ticktalkpython.Graph import TTGraph
from ticktalkpython.IPC import Message
from ticktalkpython.IPC import Recipient
from ticktalkpython.IPC import RuntimeMsg

logger = DebugLogger.get_logger('StreamMerge')

def extract_output_info_from_line(line):
    '''
    This is pure hackery.  The data should be saved from the
    simulation rather than parsing the log file.
    '''
    value = None
    source_sq = None
    source_ensemble = None
    temp = None
    if line[1:8] == 'TTToken':
        start_t_index = line.find('T:')
        value_str = line[9:start_t_index-1]
        value =  float(value_str)

        open_paren_indices = [ i.start() for i in re.finditer(r'\(', line)]
        close_paren_indices = [ i.start() for i in re.finditer(r'\)', line)]
        timestamps_str = line[open_paren_indices[-1]+1 : close_paren_indices[-1]]
        start_time = int(timestamps_str.split(',')[0])
        stop_time = int(timestamps_str.split(',')[1])
        temp = (start_time +stop_time) / 2

        start_sq_index = line.find('from SQ "')
        end_sq_index = line.find('" on ENS: "' )
        source_sq = line[start_sq_index+len('from SQ "') : end_sq_index]

        start_ensemble_index = end_sq_index
        end_ensemble_index = line.find('".') #not very unique..
        source_ensemble = line[start_ensemble_index+len('" on ENS: "') : end_ensemble_index]

    return value, source_sq, source_ensemble, temp

def process_outputs(sim=False, log_file_path=None, out_sq_names=None):
    '''
    Process outputs
    '''
    output_sq_names = ['ADD-16', 'movingAverage-17'] if out_sq_names is None else out_sq_names
    if sim:
        phy_or_sim_str = 'simulation'
    else:
        phy_or_sim_str = 'phy'
    values = [[] for i in range(len(output_sq_names))]
    times = [[] for i in range(len(output_sq_names))]
    source_ensembles = [None for i in range(len(values))]
    with open(log_file_path, 'r') as file:
        lines = file.readlines()
        header_lines = []
        for i, line in enumerate(lines):
            if f'start execution of {phy_or_sim_str} (streaming_merge)' in line:
                header_lines.append(i)

        most_recent_set = lines[header_lines[-1]:]
        for line in most_recent_set:
            val, this_sq, ens, timestamp = extract_output_info_from_line(line)
            if val is not None:
                try:
                    sq_index = output_sq_names.index(this_sq)

                    values[sq_index].append(val)
                    times[sq_index].append(timestamp)
                    source_ensembles[sq_index] = ens #assume this cannot be more than one
                except Exception as exc:
                    logger.debug("Exception %s", exc)

    logger.info(output_sq_names)
    logger.info(source_ensembles)
    for i, sq_name in enumerate(output_sq_names):
        if not source_ensembles[i] is None:
            plt.plot(times[i], values[i], 'b.')
            plt.title("'" + sq_name + "' output from Ensemble '" + source_ensembles[i] + "'")
            plt.ylabel('Token Value') #we're just going to assume numeric
            plt.xlabel('Time')
            plt.show()
        else:
            logger.info("'%s' does not have a corresponding source_ensemble", sq_name)

def unpack_graph(filename):
    '''
    Unpack graph
    '''
    inpickle = open(filename, 'rb')
    graph = pickle.load(inpickle)
    assert isinstance(graph, TTGraph)
    return graph

def send_input_tokens(graph, runtime_manager: TTRuntimeManager, inputs=None):
    '''
    Send input tokens
    '''
    logger.info('Sending input tokens part 1')
    trigger_inputs = {'trigger':0xdeadbeef} if inputs is None else inputs
    execute_graph_message = Message(
        RuntimeMsg.ExecuteGraphOnInputs,
        (graph, trigger_inputs),
        Recipient.ProcessRuntimeManager)
    logger.info('Sending input tokens part 2')
    runtime_manager.send_to_runtime(execute_graph_message)

def send_graph_sim(runtime_manager, sim, filename):
    '''
    Send graph sim
    '''
    yield sim.timeout(0)
    logger.info("Distribute Graph to ensembles")
    graph = unpack_graph(filename=filename)
    # draw_graph(graph)

    # All this does is to build a message...
    instantiate_graph_msg = Message(
        RuntimeMsg.InstantiateAndMapGraph,
        graph,
        Recipient.ProcessRuntimeManager)

    # This sends the message and should trigger the mapping of SQs to ensembles
    runtime_manager.send_to_runtime(instantiate_graph_msg)
    yield sim.timeout(0)
    logger.info('Graph should be mapped now')

    send_input_tokens(graph, runtime_manager)

def streaming_merge_sim(pickle_path, log_file_path):
    '''
    Streaming merge sim
    '''
    try:
        with open(log_file_path, 'a') as log_file:
            log_file.write(
                f'\nstart execution of simulation (streaming_merge) at {time.time()}\r\n')
            timeout = 10000000000
            logger.info('setup sim')
            sim = simpy.Environment(initial_time=0)
            logger.info('setup ensembles')
            rtm = TTRuntimeManagerSim(log_file, [], sim)
            logger.info('send graph inputs')
            sim.process(send_graph_sim(rtm, sim, filename=pickle_path))
            rtm.manager_ensemble.enter_steady_state(timeout=timeout)
            sim.run(until=timeout)
            process_outputs(sim=True, log_file_path=log_file_path)

    except KeyboardInterrupt:
        logger.info('KB interrupt; exit simulation')
    except BaseException as exc:
        logger.error(traceback.format_exc())
        raise exc

def streaming_merge_phy(pickle_path, log_file_path):
    '''
    Streaming merge PHY
    '''
    raise DeprecationWarning
    try:
        with open(log_file_path, 'a') as log_file:
            log_file.write('\nstart execution of phy (streaming_merge) at %f\r\n', time.time())

            rtm = TTRuntimeManagerPhysical(log_file, ip='127.0.0.1', rx_port=2009, tx_port=2010)
            # rtm_address = '127.0.0.1:2009'

            time.sleep(.01)
            input('wait... press enter\n\n')

            graph = unpack_graph(filename=pickle_path)
            instantiate_graph_msg = Message(
                RuntimeMsg.InstantiateAndMapGraph,
                graph,
                Recipient.ProcessRuntimeManager)
            rtm.send_to_runtime(instantiate_graph_msg)

            time.sleep(.01)

            input('wait... press enter\n\n')
            send_input_tokens(graph, rtm)

            rtm.manager_ensemble.enter_steady_state(timeout=60)

            process_outputs(sim=False)


    except KeyboardInterrupt:
        logger.info('KB interrupt; exit physical test')
    except BaseException as exc:
        traceback.print_exc(exc)
        raise

def main(pickle_path, log_file_path):
    '''
    Main
    '''
    ###Physical
    streaming_merge_phy(pickle_path, log_file_path)

    input(r'\n\n\Press enter to continue with simulated test\n\n')
    # time.sleep(5)

    logger.critical("\n\n\nFinish physical test\n\n\nStart Simulated\n")

    ###Simulated
    time_1 = time.time()
    streaming_merge_sim(pickle_path, log_file_path)
    time_2 = time.time()
    logger.info(time_2-time_1)
    logger.critical('\n*** Done ***\n')

# if __name__ == "__main__":
#     main()
