# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

'''
An alternative to compiling and running the stream_merge_test in
Jupyter.  This file can be launched and debugged in PyCharm
'''

import os
import sys
import logging

from tests.system_test import all_tests
from ticktalkpython.Compiler import TTCompile
# from ticktalkpython.Compiler import draw_graph
from ticktalkpython.Compiler import dump_json
from ticktalkpython.Compiler import dump_pickle
from ticktalkpython import DebugLogger as log

import tests.stream_merge_test as stream_merge

print("Running regression tests")
all_tests()

print(f"Working directory: {os.path.abspath('.')}")
print("Python path:")
print(sys.path)

TEST_NAME = "streaming_merge"
INPUT_SUBDIRECTORY = "../examples"
relative_python_path = f"{INPUT_SUBDIRECTORY}/{TEST_NAME}.py"
abs_path = os.path.abspath(relative_python_path)    # e.g., /home/username/foo.py
dir_name = os.path.dirname(abs_path)                # e.g., /home/username
base_name = os.path.basename(abs_path)              # e.g., foo.py
split_name = base_name.split('.')
length = len(split_name)
if split_name[-1] == 'py':
    # 'py': 1    'foo.py': 1   'foo.bar.py': 2
    n_parts_to_keep = max(1, length - 1)
else:
    n_parts_to_keep = length
BASE_NO_EXTENSION = '.'.join(base_name.split('.')[:n_parts_to_keep])
input_python_path = dir_name + '/' + BASE_NO_EXTENSION + '.py'

home_directory = os.getenv("HOME")
desktop_directory_path = f"{home_directory}/Desktop"
output_json_path = f"{desktop_directory_path}/{TEST_NAME}.json"
output_pickle_path = f"{desktop_directory_path}/{TEST_NAME}.pickle"
log_path = f"{desktop_directory_path}/{TEST_NAME}.log"

print(f"Compiling {input_python_path}")
log.set_base_logger_level(logging.WARNING)
graph = TTCompile(input_python_path)
# draw_graph(graph)

print(f"Compilation done for {input_python_path}")

dump_json(graph, output_json_path)
dump_pickle(graph, output_pickle_path)
log.set_base_logger_level(logging.INFO)

stream_merge.streaming_merge_sim(output_pickle_path, log_path)
