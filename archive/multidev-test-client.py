# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import sys
import random

import Ensemble
from IPC import *
import DebugLogger
import logging
DebugLogger.set_base_logger_level(logging.INFO)
logger = DebugLogger.get_logger('test.runtime-manager')

def run_application_client(self_ip, self_port, rtm_ip, rtm_port):

    rtm_address = rtm_ip+":"+str(rtm_port)

    ens1 = Ensemble.TTEnsemble('ens'+str(random.randint(0, 10)))
    ens1.setup_queues(is_sim=False)
    ens1.setup_physical_processes(network_ip=self_ip, rx_network_port = self_port, tx_network_port = self_port+1)
    ens1.connect_to_TickTalk_network(rtm_address)

    ens1.enter_steady_state(timeout=90)


def port_and_ip(string):
    ip = string.trim().split(':')[0]
    port = int(string.trim().split(':')[1])

    return ip, port

if __name__ == "__main__":
    argc = len(sys.argv)
    self_ip, self_port, rtm_ip, rtm_port = None, None, None, None
    if argc == 3:
        self_ip, self_port = port_and_ip(sys.argv[1])
        rtm_ip, rtm_port = port_and_ip(sys.argv[2])
    elif argc == 5:
        self_ip = sys.argv[1]
        self_port = int(sys.argv[2])
        rtm_ip = sys.argv[3]
        rtm_port = int(sys.argv[4])
    else:
        raise ValueError('gonna need that ip and port (for self and runtime manager) as CLI args, dawg.')

    input('\n\n\nHit enter to start test\n\n')

    run_application_client(self_ip, self_port, rtm_ip, rtm_port)
