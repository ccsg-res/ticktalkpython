# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


from Time import TTTime
from Clock import TTClock
from TTToken import TTToken
from SQ import *
from SQSync import *

def match_game():
    # Allocate a TokenStorage instance for a 2-port SQ
    token_storage = TTSQInputTokenStorage(2, SQ.TTFiringRuleType.Timed, 'Test-SQ-0')
    # Conjure up the root clock
    root_clock = TTClock.root()
    # Create a timestamp
    time1_root = TTTime(root_clock, 0, 2)
    # Create a timestamp
    time2_root = TTTime(root_clock, 1, 3)
    # Create two different but identically-timed tokens
    token1 = TTToken(1, time1_root)
    token2 = TTToken(2, time2_root)
    # Queue up a token on port 0
    token_storage.insert_token(token1, 0)
    # See if it matches the queued token
    overlap_time, token_list = token_storage.match_token(token2, 1)
    assert(overlap_time.start_tick == 1)
    assert(overlap_time.stop_tick == 2)
    assert(token_list[0] == token1)
    assert(token_list[1] == token2)

    other_clock = TTClock("other clock", root_clock, 3, 19)
    time3_other = TTTime(other_clock, 0, 1)
    # IMPORTANT: mark this as having come from a streaming source
    token3 = TTToken(17, time3_other, streaming=True)
    token_storage2 = TTSQInputTokenStorage(2, SQ.TTFiringRuleType.Timed)
    token_storage2.insert_token(token1, 1)
    try:
        # Should flag a clock mismatch error
        overlap_time, token_list = token_storage2.match_token(token3, 0)
        raise Exception("UntriggeredError", "Illegal token matching should have triggered an error but did not")
    except Exception as error:
        type, description = error.args
        assert(type == "WaitingMatchingError")

def all_tests():
    match_game()
    # Will only get here if there are no assertion failures
    print("All token matching tests PASSED")

if __name__ == "__main__":
    all_tests()
