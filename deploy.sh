#!/bin/sh

tar -czvf deploy_tt.tar.gz ticktalkpython/ requirements_base.txt compile.py \
    runens.py runrtm.py output_functions.py
