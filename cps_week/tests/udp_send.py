# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import sys
import os

from ticktalkpython import Network
from ticktalkpython import Tag
from ticktalkpython import TTToken
import time
file = open("./time_instrument.py")
contents = file.read()
file.close()

tokenA = Token.Token(Tag.Tag("foo", 'arg1', 0, 100), "I'm token A!")
tokenB = Token.Token(Tag.Tag("foo", 'arg1', 100, 200), "I'm token B! This is my value")
tokenC = Token.Token(Tag.Tag("foo", 'arg2', 100, 200), "I'm token C! This is my value")

network = TTNetwork(port=3031)

tokenAMsg = Network.message(tokenA, TTMessageType.InputToken, recipient_port=3030)
tokenBMsg = Network.message(tokenB, TTMessageType.InputToken, recipient_port=3030)
tokenCMsg = Network.message(tokenC, TTMessageType.InputToken, recipient_port=3030)
send_num = 0

while(True):
    tokenAMsg = Network.message(tokenA, TTMessageType.InputToken, recipient_port=3030)
    print("%s: Sending token A, ID=%s, value=%s" % (send_num, id(tokenA), tokenA.value))
    network.send(tokenAMsg)
    send_num += 1
    time.sleep(1/2)

#print("Sending token B, ID=%s, value=%s" % (id(tokenB), tokenB.value))
#network.send(tokenBMsg)

#print("Sending token C, ID=%s, value=%s" % (id(tokenC), tokenC.value))
#network.send(tokenCMsg)