TTSQ
====


The SQ is one of the most foundational concepts in TTPython, and has two
essential subclasses, :ref:`TTSQSync<sqsync>` and :ref:`TTSQExecute<sqexecute>`
for handling the intricacies of synchronization and execution. The Forwarding
section is represented using an array of ``TTArcDestination``.


.. automodule:: ticktalkpython.SQ
   :members:
