TTCompile
==========

.. _compiler:


.. note:: ``@SQify``-ed functions should be created at top level (non-indented) and must only contain *legitimate* ``SQ`` bodies per TickTalk rules.

.. automodule:: ticktalkpython.Compiler
   :members:

