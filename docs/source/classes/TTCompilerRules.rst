TTCompilerRules
===============

``TTCompilerRules`` defines how the python abstract syntax tree (AST) library should handle various syntactic elements as it visits each node of the AST.

.. automodule:: ticktalkpython.CompilerRules
   :members:
