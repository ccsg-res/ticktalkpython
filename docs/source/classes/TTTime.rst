TTTime
======

The ``TTTime`` class implements the basics of defining intervals.

:clock:  object of ``TTClock`` type
:start_tick:  integer representing the first tick in the interval
:stop_tick:  integer representing the first tick beyond the interval

Every valid ``TTTime`` spans at least one tick of its clock.

.. automodule:: ticktalkpython.Time
   :members:
