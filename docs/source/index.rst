.. TTPython documentation master file, created by
   sphinx-quickstart on Thu Feb 18 14:59:03 2021.

TTPython
========

TTPython is a language and programming environment aimed at simplifying
development of applications for large-scale, distributed, time-aware,
energy-constrained systems such as smart cities and smart environments.
TTPython includes a Python-derived programming language, a compiler, a graph
interpreter, and interfaces to a cyber-physical simulator.  Developing TTPython
applications begins with understanding the core concepts of the language and the
intended graph-based runtime systems. `Please find the TTPython Source Code on
Bitbucket <https://bitbucket.org/ccsg-res/ticktalkpython/src/master/>`_.

.. toctree::
   :maxdepth: 3
   :hidden:

   overview
   coreconcepts
   tutorial-index
   advanced-index
   manual-index





