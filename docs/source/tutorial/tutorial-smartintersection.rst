Tutorial - Intersecting Concepts
================================

.. _tutorial-smartintersection:

.. comment:
    "Wow, that was a great pun, Reese."

    "Thanks, you should see what I have to say about energy and time sync."

    Title: "Time is Money - The (Energy) Cost of Precise Time Synchronization"

If you've made it this far, congratulations -- you are now familiar with many
of the foundational TTPython concepts, so it's time to show their intersection
in an application for Smart Intersection, the very same that won the TTPython
Dev Team 2nd place at the CPS-IoT Week Student Design Competition, Networked
Computing on the Edge.

`You can find our presentation on Youtube <https://youtu.be/f5HHljXT6DY>`_
