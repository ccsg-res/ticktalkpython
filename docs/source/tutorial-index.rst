********
Tutorial
********

.. toctree::
   :maxdepth: 2
   :hidden:

   tutorial/tutorial-install
   tutorial/tutorial-intro
   tutorial/tutorial-sqify
   tutorial/tutorial-streamify
   tutorial/tutorial-mapping
   tutorial/tutorial-deadlines
   tutorial/tutorial-smartintersection
   tutorial/tutorial-considerations


Rather than making newcomers wade through APIs and reference docs in order to
understand the key ideas, we've provided a series of tutorials to help new
programmers gradually learn about the syntax and semantics of TTPython

This tutorial provides a walkthrough of most fundamental concepts and mechanisms
in increasing complexity, culminating with an application based on a 1/10th
scale Smart Intersection Application that won the TTPython Dev Team 2nd place at
the CPS-IoT Week Student Design Competition, Networked Computing on the Edge.

We also provide a demonstration video to show TTPython running across several
Ensembles at once, executing a TTPython produced graph in parallel. Please find
this `YouTube video here <https://www.youtube.com/watch?v=xCLy89LLpaw>`_.
