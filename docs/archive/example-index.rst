********
Examples
********

.. toctree::
   :maxdepth: 2
   :hidden:

   example1
   example2
   anatomy


@Deprecated: this information will be redefined within the Tutorial pages

It is useful to see these concepts together in a few TTPython programs.  In the first example, we start with *dusty deck* Python and convert it to TTPython, albeit in a form that expresses no parallelism (and, therefore, not too suitable for distribution over many IoT nodes).  In the second example, we rewrite the same logic in a form that naturally exposes parallelism to highlight the differences.