Review
======

If you've read this far, congratulations.  You now have a basic knowledge of TTPython's objectives, approach, and core elements.  Take a few minutes to review the figure below and make sure the concepts are clear before proceeding.

.. image:: ./_static/anatomy.jpg
  :width: 800