*********
Libraries
*********

.. toctree::
   :maxdepth: 2
   :hidden:

   ../sqify
   ../instructions

The TTPython library scheme is currently under development.  

Library functions and vanilla Python functions of your own within a graph are or must be ``@SQify``-ed.

The library of ``@SQify``-ed primitive instructions is provided to enable basic arithmetic operations on number-valued tokens using traditional infix notation.