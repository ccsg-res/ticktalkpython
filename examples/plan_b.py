# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

from ticktalkpython.Deadline import TTFinishByOtherwise
from ticktalkpython.Instructions import READ_TTCLOCK
from ticktalkpython.SQ import GRAPHify, SQify
from ticktalkpython.Clock import TTClock


@SQify
def timed_add1(x):
    import time
    time.sleep(0.2)
    return x + 1


@SQify
def const_val():
    return 0


@GRAPHify
def ifelse(trigger):
    a = 1
    with TTClock('ROOT') as root_clock:
        t = READ_TTCLOCK(trigger, TTClock=root_clock) + 500
        y = timed_add1(a) * 3
        # if deadline fails, it produces a separate value (similar to ternary
        # operation: a = y if clock.now < t else None)
        q = TTFinishByOtherwise(y,
                                TTTimeDeadline=t,
                                TTPlanB=const_val(),
                                TTWillContinue=True)
        return q / 3
