# Copyright 2021 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

from ticktalkpython.Time import TTTime
from ticktalkpython.Clock import TTClock
from ticktalkpython.TTToken import TTToken
from ticktalkpython.SQ import SQify
from ticktalkpython.SQ import GRAPHify
from ticktalkpython.Instructions import *
from ticktalkpython.Deadline import TTDeadline
from ticktalkpython.PlanB import TTPlanB
from ticktalkpython.Query import TTConstraint
from math import sqrt

@SQify
def quadratic_roots(a, b, c):
    sqrt_term = sqrt(b**2 - 4 * a * c)
    a_times_2 = 2 * a
    return ((-b + sqrt_term) / a_times_2, (-b - sqrt_term) / a_times_2)

@GRAPHify
def main(trigger):
    a = 1
    b = 2
    c = 1
    with TTClock.root() as CLOCK:
        with TTConstraint(name="dev1"):
            sqrt_term = SQRT((b * b) - 4 * a * c)
            a_times_2 = 2 * a
        with TTConstraint(name="camera"):
            root_1 = (-b + sqrt_term) / a_times_2
            root_2 = (-b - sqrt_term) / a_times_2
        return TUPLE_2(root_1, root_2)
